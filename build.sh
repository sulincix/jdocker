#!/bin/bash
jdir="jdk-19.0.1"
if [ ! -f java.tgz ] ; then
    wget https://download.java.net/java/GA/jdk19.0.1/afdd2e245b014143b62ccb916125e3ce/10/GPL/openjdk-19.0.1_linux-x64_bin.tar.gz -O java.tgz
fi
if [ ! -d jdk-19.0.1 ] ; then
    tar -xvf java.tgz
fi
cd $jdir
export LD_LIBRARY_PATH=./lib:./lib/server
ln -s lib lib64
find . -type f -exec ldd {} \; |& grep "=>" | grep -v "=> ./" | cut -f3 -d" " | sort | uniq | sed "s/.*/install & .\/lib/g" | sh
install /lib64/ld-linux-x86-64.so.2 lib
cd ..
tar -cv -C $jdir . | docker import -
